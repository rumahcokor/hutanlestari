<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('information')->group(function (){
    Route::get('/informasi' , [\App\Http\Controllers\informationController::class , 'index'])->name('informasi.index');
    Route::get('/flora/{id}' , [\App\Http\Controllers\informationController::class , 'flora'])->name('informasi.flora');
    Route::get('/fauna/{id}' , [\App\Http\Controllers\informationController::class , 'fauna'])->name('informasi.fauna');
});

Route::prefix('campaign')->group(function (){
    Route::get('/index' ,[\App\Http\Controllers\campaignController::class , 'index'])->name('campaign.index');
    Route::get('/formcampaign' ,  [\App\Http\Controllers\campaignController::class , 'form'])->name('campaign.form')->middleware(['auth']);
    Route::post('/formcampaign' , [\App\Http\Controllers\campaignController::class , 'formpost'])->name('campaign.form')->middleware(['auth']);
    Route::get('/detail/{id}' , [\App\Http\Controllers\campaignController::class , 'detail'])->name('campaign.detail');
    Route::get('/donasi/{id}' , [\App\Http\Controllers\campaignController::class , 'donasi'])->name('campaign.donasi')->middleware(['auth']);
    Route::post('/donasi/{id}' , [\App\Http\Controllers\campaignController::class , 'donasipost'])->name('campaign.donasi')->middleware(['auth']);
    Route::get('/volunteer/{id}' , [\App\Http\Controllers\campaignController::class , 'volunteer'])->name('campaign.volunteer')->middleware(['auth']);
    Route::post('/volunteer/{id}' , [\App\Http\Controllers\campaignController::class , 'volunteerpost'])->name('campaign.volunteer')->middleware(['auth']);

    Route::get('/donasiflorafauna' , [\App\Http\Controllers\campaignController::class , 'donasiflorafauna'])->name('campaign.donasiflorafauna')->middleware(['auth']);
    Route::post('/florafaunapost' , [\App\Http\Controllers\campaignController::class , 'florafaunapost'])->name('campaign.florafaunapost');

});


Route::prefix('admin')->group(function (){
            Route::get('/verifikasidana', [\App\Http\Controllers\adminController::class , 'verifdana'])->name('admin.verifdana');
            Route::get('/verifikasidanapost/{id}' , [\App\Http\Controllers\adminController::class , 'verifdanapost'])->name('admin.verifdanapost');
            Route::get('/verifikasidanatolak/{id}' , [\App\Http\Controllers\adminController::class , 'tolakdana'])->name('admin.tolakdana');
            Route::get('/verifikasivoluntolak/{id}' , [\App\Http\Controllers\adminController::class , 'tolakvolun'])->name('admin.tolakvolun');

            Route::get('/verifikasivolunteer' , [\App\Http\Controllers\adminController::class , 'verifvolun'])->name('admin.verifvolun');
            Route::get('/verifikasivolunteerpost/{id}' , [\App\Http\Controllers\adminController::class , 'verifvolunpost'])->name('admin.volunpost');

            Route::get('/verifikasicampaign' , [\App\Http\Controllers\adminController::class , 'verifcampaign'])->name('admin.verifcampaign');
            Route::get('/verifikasicampaign/{id}' , [\App\Http\Controllers\adminController::class , 'verifcampaignpost'])->name('admin.verifcampaignpost');
            Route::get('/verifikasicampaigntolak/{id}' , [\App\Http\Controllers\adminController::class , 'verifcampaigntolak'])->name('admin.verifcampaigntolak');

            Route::get('/verifikasiflora' , [\App\Http\Controllers\adminController::class , 'verifikasiflora'])->name('admin.verifflora');
            Route::get('verifikasiflora/{id}' , [\App\Http\Controllers\adminController::class , 'verifflorapost'])->name('admin.verifflorapost');
            Route::get('tolakflora/{id}' , [\App\Http\Controllers\adminController::class , 'tolakflora'])->name('admin.tolakflora');

            Route::get('/verifblog' , [\App\Http\Controllers\adminController::class , 'blog'])->name('admin.verifblog');
            Route::get('/verifblog/{id}' , [\App\Http\Controllers\adminController::class , 'blogverif'])->name('admin.verifblogpost');
             Route::get('/tolakblog/{id}' , [\App\Http\Controllers\adminController::class , 'tolakblog'])->name('admin.tolakblog');

             Route::get('/input/informasifauna' , [\App\Http\Controllers\adminController::class , 'informasifaunaform'])->name('admin.informasifaunaform');
             Route::post('/input/informasifauna' , [\App\Http\Controllers\adminController::class , 'informasifaunaformpost'])->name('admin.informasifaunaform');
             Route::get('/input/deleteinformasifauna/{id}' , [\App\Http\Controllers\adminController::class , 'deleteinfromasifauna'])->name('admin.deleteinfromasifauna');
            Route::get('/input/deleteinformasiflora/{id}' , [\App\Http\Controllers\adminController::class , 'deleteinformasiflora'])->name('admin.deleteinformasiflora');

            Route::get('/input/informasifaunaedit/{id}' , [\App\Http\Controllers\adminController::class , 'informasifaunaedit'])->name('admin.informasifaunaedit');

             Route::post('/input/informasifaunaedit/{id}' , [\App\Http\Controllers\adminController::class , 'informasifaunaeditpost'])->name('admin.informasifaunaeditpost');


    Route::get('/input/informasiflora' , [\App\Http\Controllers\adminController::class , 'informasifloraform'])->name('admin.informasifloraform');
    Route::get('/input/informasifloraedit/{id}' , [\App\Http\Controllers\adminController::class , 'informasifloraedit'])->name('admin.informasifloraedit');
    Route::post('/input/informasifloraedit/{id}' , [\App\Http\Controllers\adminController::class , 'informasifloraeditpost'])->name('admin.informasifloraeditpost');
    Route::post('/input/informasiflora' , [\App\Http\Controllers\adminController::class , 'informasifloraformpost'])->name('admin.informasifloraform');
});


Route::prefix('dashboard')->group(function (){
        Route::get('/campaign' , [\App\Http\Controllers\dashboardController::class , 'volun'])->name('dashboard.volun');
        Route::get('/dana' , [\App\Http\Controllers\dashboardController::class , 'dana'])->name('dashboard.dana');
        Route::get('/florafauna' , [\App\Http\Controllers\dashboardController::class , 'flora'])->name('dashboard.flora');
        Route::get('/settingss' , [\App\Http\Controllers\dashboardController::class , 'setting'])->name('settings');
        Route::post('/settings/{id}' , [\App\Http\Controllers\dashboardController::class , 'settings'])->name('settingspost');
        Route::get('/changepassword' , [\App\Http\Controllers\dashboardController::class , 'changepassword'])->name('changepassword');
        Route::post('/changepassword' , [\App\Http\Controllers\dashboardController::class , 'changepasswordpost'])->name('changepassword');
        Route::get('/blog' , [\App\Http\Controllers\dashboardController::class , 'blog'])->name('dashboard.blog');

});

Route::prefix('blog')->group(function (){
        Route::get('/formblog' , [\App\Http\Controllers\blogController::class , 'form'])->name('blog.form')->middleware(['auth']);
    Route::post('/formblog' , [\App\Http\Controllers\blogController::class , 'formpost'])->name('blog.form')->middleware(['auth']);
    Route::get('/index' , [\App\Http\Controllers\blogController::class , 'index'])->name('blog.index');
    Route::get('/detail/{id}' , [\App\Http\Controllers\blogController::class , 'detail'])->name('blog.detail');
});

Route::get('/feedback' , [\App\Http\Controllers\dashboardController::class , 'feedback'])->name('feedback');
Route::post('/feedback' , [\App\Http\Controllers\dashboardController::class , 'feedbackpost'])->name('feedback');

Route::get('/ourteam', function () {
    return view('ourteam');
})->name('ourteam');


