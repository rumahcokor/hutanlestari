@extends('layouts.dashboardadmin')

@section('content')

    <div class="card mt-3 p-3 mr-4 bg-success">
        <div class="card-head bg-success ">
            <h4 class="text-black text-center p-2 bg-white" >Verifikasi Campaign</h4>
        </div>

        <div class="card-body bg-white">
        <table class="table table-responsive table-striped bg-white overflow-scroll" id="table_id" class="display">
            <thead>
            <tr>
                <th scope="col">Judul</th>
                <th scope="col">Type</th>
                <th scope="col">Start Date</th>
                <th scope="col">Finish Date</th>
                <th scope="col">Target</th>
                <th scope="col">Detail</th>
                <th scope="col">Status</th>
                <th scope="col">Aksi</th>
            </tr>
            </thead>
            <tbody class="overflow-scroll">
                @foreach($data as $x)
                    <tr>
                    <td>{{$x->nama_campaign}}</td>
                    @if($x->volunteer_check = 1 && $x->donation_check == 1)
                        <td>Volunteer dan donasi</td>
                    @elseif($x->volunteercheck = 1 && $x->donation_check == 0)
                        <td>volunteer</td>
                    @else
                        <td>Donasi</td>
                    @endif
                    <td>{{$x->start_date}}</td>
                    <td>{{$x->end_date}}</td>
                    <td>{{$x->target}}</td>
                    <td>
                        <button class="btn btn-info" data-toggle="modal" data-target="#exampleModal{{$x->nama_campaign}}">Detail</button>

                    </td>
                    <td>
                  @if($x->verifikasi_check == 0 )
                      <p class="text-danger"> Belum Di verifikasi</p>
                  @else
                      <p class="text-success"> sudah Di verifikasi</p>
                  @endif
                    </td>

                    <td>
                        @if($x->verifikasi_check == 0 )
                            <div class="d-flex">
                              <a href="{{route('admin.verifcampaignpost' , ['id' => $x->id])}}" class="btn btn-success mr-2"> Verifikasi</a>
                              <a href="{{route('admin.verifcampaigntolak' , ['id'=> $x->id])}}" class="btn btn-danger"> Tolak Verifikasi</a>
                            </div>
                        @else
                            <a href="{{route('admin.verifcampaigntolak' , ['id'=> $x->id])}}" class="btn btn-danger"> Delete</a>

                        @endif

                    </td>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal{{$x->nama_campaign}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{$x->nama_campaign}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <img class="d-block mx-auto" src="{{asset('/campaignimage/'.$x->img)}}" alt="">
                                </div>
                                <div class="modal-body">
                                    <p>{{$x->deskripsi_campaign}}</p>
                                    @if($x->volunteer_check = 1 && $x->donation_check == 1)
                                        <p> Type : Volunteer dan donasi</p>
                                    @elseif($x->volunteercheck = 1 && $x->donation_check == 0)
                                        <p>Type : volunteer</p>
                                    @else
                                        <p>Type : Donasi</p>
                                    @endif
                                    <p>Tanggal Mulai : {{$x->start_date}}</p>
                                    <p>Tanggal Selesai : {{$x->end_date}}</p>
                                    <p>Target : {{$x->target}}</p>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </tr>
                @endforeach

            </tbody>

        </table>
        </div>




    </div>

@endsection

@push('script')

    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
@endpush
