@extends('layouts.app')

@section('content')

    <div class="container">
        <h2 class="text-center text-success my-5">
            Our Team
        </h2>

        <div class="mt-1 bg-card">
            <div class="d-flex justify-content-center align-content-center">
                <div class="m-4 align-content-center">
                    <img class="d-block mx-auto" src="{{asset('gmbr/muka1.png')}}" style="width: 50%" alt="">
                    <p class="align-content-end text-center">Siti Alifah Hartaniah</p>
                    <p class="align-content-end text-center">1202190126</p>
                    <p class=" text-center">Posisi</p>
                </div>

                <div class="m-4 align-content-center">
                    <img class="d-block mx-auto" src="{{asset('gmbr/muka2.png')}}" style="width: 50%" alt="">
                    <p class="align-content-end text-center">Regina Tasya Widiharlina</p>
                    <p class="align-content-end text-center">NIM</p>
                    <p class=" text-center">Posisi</p>
                </div>

                <div class="m-4 align-content-center">
                    <img class="d-block mx-auto" src="{{asset('gmbr/mukakosong.png')}}" style="width: 50%" alt="">
                    <p class="align-content-end text-center">Vina Fadillah</p>
                    <p class="align-content-end text-center">1202194208</p>
                    <p class=" text-center">Posisi</p>
                </div>
            </div>

            <div class="mt-3 d-flex justify-content-center align-content-center">
                <div class="m-4 align-content-center">
                    <img class="d-block mx-auto" src="{{asset('gmbr/muka3.png')}}" style="width: 50%" alt="">
                    <p class="align-content-end text-center">Ahmad Dzihan Kamaly</p>
                    <p class="align-content-end text-center">NIM</p>
                    <p class=" text-center">Posisi</p>
                </div>

                <div class="m-4 align-content-center">
                    <img class="d-block mx-auto" src="{{asset('gmbr/muka3.png')}}" style="width: 50%" alt="">
                    <p class="align-content-end text-center">Emas Dai</p>
                    <p class="align-content-end text-center">NIM</p>
                    <p class=" text-center">Posisi</p>
                </div>


            </div>

        </div>
    </div>



@endsection

