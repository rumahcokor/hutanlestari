@extends('layouts.app')

@section('content')
    <div class="container">
        <h3 class="text-center text-success my-3">
            Campaign Terkini
        </h3>
        <div class="row">
            <div class="col">

            </div>

            <?php $x=0?>
            @foreach($data as $x)
            <div class="col-3 mx-3">
                <div class="card rounded-3 shadow" style="width: 18rem;">
                    <div class="shadow" style="height: 200px;overflow: hidden" >
                        <img src="{{asset('campaignimage/'.$x->img)}}" class="img-fluid" alt="...">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{$x->nama_campaign}}</h5>
                        <p class="text-black-50"><svg class="mr-2" width="16" height="20" viewBox="0 0 16 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M15.84 16.1931C15.84 19.4891 11.32 19.8701 7.921 19.8701L7.67776 19.8699C5.51221 19.8646 0 19.7279 0 16.1731C0 12.9444 4.33835 12.5129 7.71148 12.4966L8.16423 12.4963C10.3296 12.5016 15.84 12.6383 15.84 16.1931ZM7.921 13.9961C3.66 13.9961 1.5 14.7281 1.5 16.1731C1.5 17.6311 3.66 18.3701 7.921 18.3701C12.181 18.3701 14.34 17.6381 14.34 16.1931C14.34 14.7351 12.181 13.9961 7.921 13.9961ZM7.921 -0.000305176C10.849 -0.000305176 13.23 2.38169 13.23 5.30969C13.23 8.23769 10.849 10.6187 7.921 10.6187H7.889C4.967 10.6097 2.6 8.22669 2.60997 5.30669C2.60997 2.38169 4.992 -0.000305176 7.921 -0.000305176ZM7.921 1.42769C5.78 1.42769 4.03798 3.16869 4.03798 5.30969C4.031 7.44369 5.76 9.18369 7.892 9.19169L7.921 9.90569V9.19169C10.061 9.19169 11.802 7.44969 11.802 5.30969C11.802 3.16869 10.061 1.42769 7.921 1.42769Z" fill="#808080"/>
                            </svg>

                            {{$x->volunteer_terkini}}</p>

                        <p class="card-text mb-1 text-black-50"> <svg width="14" class="mr-2" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M7 1V23" stroke="#808080" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M12 5H4.5C3.57174 5 2.6815 5.36875 2.02513 6.02513C1.36875 6.6815 1 7.57174 1 8.5C1 9.42826 1.36875 10.3185 2.02513 10.9749C2.6815 11.6313 3.57174 12 4.5 12H9.5C10.4283 12 11.3185 12.3687 11.9749 13.0251C12.6313 13.6815 13 14.5717 13 15.5C13 16.4283 12.6313 17.3185 11.9749 17.9749C11.3185 18.6313 10.4283 19 9.5 19H1" stroke="#808080" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>

                            Rp.{{$x->target}}</p>
                        <?php

                         $y =    \Carbon\Carbon::now()->format('Y-m-d');
                         $t = \Carbon\Carbon::parse($x->end_date)->diffInDays($y);
                        ?>
                        <p style="text-align: right">{{$t}} Hari Lagi</p>
                        <div class="progress">
                            <div class="progress-bar bg-success"  style="width: <?php
                            if ($x->donasi_terkini >= $x->target){
                                echo "100%";
                            }else {
                              $lebar =  $x->donasi_terkini / $x->target * 100;
                              echo $lebar."%";
                            }
                            ?>;" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <a href="{{route('campaign.detail' , ['id'=>$x->id])}}" class="btn btn-outline-success w-100 mt-3">Detail Campaign</a>
                    </div>
                </div>
            </div>
                @endforeach

            <div class="col"></div>
        </div>

        <div class="d-flex align-content-between mt-2 justify-content-between">
            <h3 class="text-success " style="margin-left: 150px">Campaign</h3>
            <a href="{{route('campaign.form')}}" class="text-center btn btn-success">+ Buat Campaign</a>
        </div>



        <div class="d-flex container flex-wrap  align-content-between justify-content-center">
            <?php $x=0?>
            @foreach($pag as $x)
                <div class="mx-1 w-25 my-3">
                    <div class="card rounded-3 shadow">
                        <div class="rounded-3 shadow" style="height: 200px;overflow: hidden" >
                            <img src="{{asset('campaignimage/'.$x->img)}}" class="img-fluid rounded" alt="...">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{$x->nama_campaign}}</h5>

                            <?php

                            $y =    \Carbon\Carbon::now()->format('Y-m-d');
                            $t = \Carbon\Carbon::parse($x->end_date)->diffInDays($y);
                            ?>

                            @if($x->donation_check != 0)

                            <div class="progress">
                                <div class="progress-bar bg-success"  style="width: <?php
                                if ($x->donasi_terkini >= $x->target){
                                    echo "100%";
                                }else {
                                    $lebar =  $x->donasi_terkini / $x->target * 100;
                                    echo $lebar."%";
                                }
                                ?>;" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>

                            <p class="card-text mb-1 text-black-50 mt-2" style="float: left"> <svg width="14" class="mr-2" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M7 1V23" stroke="#808080" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M12 5H4.5C3.57174 5 2.6815 5.36875 2.02513 6.02513C1.36875 6.6815 1 7.57174 1 8.5C1 9.42826 1.36875 10.3185 2.02513 10.9749C2.6815 11.6313 3.57174 12 4.5 12H9.5C10.4283 12 11.3185 12.3687 11.9749 13.0251C12.6313 13.6815 13 14.5717 13 15.5C13 16.4283 12.6313 17.3185 11.9749 17.9749C11.3185 18.6313 10.4283 19 9.5 19H1" stroke="#808080" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>

                                Rp.{{$x->target}}</p>
                            @else

                                <div class="progress">
                                    <div class="progress-bar bg-success"  style="width: <?php
                                    if ($x->volunteer_terkini >= $x->target_volunteer){
                                        echo "100%";
                                    }else {
                                        $lebar =  $x->volunteer_terkini / $x->target_volunteer * 100;
                                        echo $lebar."%";
                                    }
                                    ?>;" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>

                                <p class="text-black-50 mt-2" style="float: left"><svg class="mr-2" width="16" height="20" viewBox="0 0 16 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M15.84 16.1931C15.84 19.4891 11.32 19.8701 7.921 19.8701L7.67776 19.8699C5.51221 19.8646 0 19.7279 0 16.1731C0 12.9444 4.33835 12.5129 7.71148 12.4966L8.16423 12.4963C10.3296 12.5016 15.84 12.6383 15.84 16.1931ZM7.921 13.9961C3.66 13.9961 1.5 14.7281 1.5 16.1731C1.5 17.6311 3.66 18.3701 7.921 18.3701C12.181 18.3701 14.34 17.6381 14.34 16.1931C14.34 14.7351 12.181 13.9961 7.921 13.9961ZM7.921 -0.000305176C10.849 -0.000305176 13.23 2.38169 13.23 5.30969C13.23 8.23769 10.849 10.6187 7.921 10.6187H7.889C4.967 10.6097 2.6 8.22669 2.60997 5.30669C2.60997 2.38169 4.992 -0.000305176 7.921 -0.000305176ZM7.921 1.42769C5.78 1.42769 4.03798 3.16869 4.03798 5.30969C4.031 7.44369 5.76 9.18369 7.892 9.19169L7.921 9.90569V9.19169C10.061 9.19169 11.802 7.44969 11.802 5.30969C11.802 3.16869 10.061 1.42769 7.921 1.42769Z" fill="#808080"/>
                                    </svg>

                                    {{$x->target_volunteer}}</p>

                            @endif

                            <p style="text-align: right" class="mt-2 text-black-50">{{$t}} More Day</p>
                            <a href="{{route('campaign.detail' , ['id'=>$x->id])}}" class="btn btn-outline-success w-100 mt-1">Detail Campaign</a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        <div class="d-flex justify-content-center">
             {{$pag->links()}}
        </div>
    </div>
@endsection
